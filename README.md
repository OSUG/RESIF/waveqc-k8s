# Update secrets with this editor

    sops helm-values/staging/secrets.yaml

# Deploy with helm

    helm secrets upgrade -i staging-waveqc waveqc -f helm-values/staging/values.yaml -f helm-values/staging/secrets.yaml

ajouter si nécessaire : 

    --kubeconfig .kube/configs/config_cluster_staging.yml
